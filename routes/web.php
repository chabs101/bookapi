<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/test', function () {
    return view('test');
});

Route::get('/web/react-native/login/authenticate','react_native\AuthController@authenticate');
Route::post('/web/react-native/user/store','react_native\UserController@store');

Route::post('/web/react-native/user/change-information','react_native\UserController@changeInfo');
Route::post('/web/react-native/user/change-password','react_native\UserController@changePassword');
Route::get('/web/react-native/user/{id}/delete', 'react_native\UserController@destroy');
Route::get('/web/react-native/logout','react_native\AuthController@logout');

Route::get('/web/react-native/books/get-list','react_native\BookController@getList');
Route::post('/web/react-native/books/store','react_native\BookController@store');
Route::post('/web/react-native/books/{id}/delete', 'react_native\BookController@destroy');

Route::get('/web/react-native/category/get-list','react_native\CategoryController@getList');
Route::post('/web/react-native/category/store','react_native\CategoryController@store');
Route::post('/web/react-native/category/{id}/delete', 'react_native\CategoryController@destroy');

Route::get('/web/react-native/chat/message/{sender_id}/{receiver_id}/get-messages','react_native\MessageController@getMessages');
Route::post('/web/react-native/chat/message/store','react_native\MessageController@store');
Route::post('/web/react-native/chat/message/{id}/delete', 'react_native\MessageController@destroy');