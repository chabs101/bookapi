<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'prefix' => 'auth'
], function () {
	Route::post('/react-native/login/authenticate','react_native\AuthController@authenticate')->name('login');
	Route::post('/react-native/user/store','react_native\UserController@store');


    Route::group([
      'middleware' => 'auth:api'
    ], function() {
		Route::get('/react-native/user/{id}/basic-info','react_native\UserController@getBasicInfo');
		Route::post('/react-native/user/change-information','react_native\UserController@changeInfo');
		Route::post('/react-native/user/change-password','react_native\UserController@changePassword');
		Route::post('/react-native/user/{id}/delete', 'react_native\UserController@destroy');
		Route::get('/react-native/logout','react_native\AuthController@logout');

		Route::get('/react-native/books/get-list','react_native\BookController@getList');
		Route::post('/react-native/books/store','react_native\BookController@store');
		Route::post('/react-native/books/{id}/delete', 'react_native\BookController@destroy');

		Route::get('/react-native/category/get-list','react_native\CategoryController@getList');
		Route::post('/react-native/category/store','react_native\CategoryController@store');
		Route::post('/react-native/category/{id}/delete', 'react_native\CategoryController@destroy');

		Route::get('/react-native/chat/message/{sender_id}/{receiver_id}/get-messages','react_native\MessageController@getMessages');
		Route::post('/react-native/chat/message/store','react_native\MessageController@store');
		Route::post('/react-native/chat/message/{id}/delete', 'react_native\MessageController@destroy');
		});
    });