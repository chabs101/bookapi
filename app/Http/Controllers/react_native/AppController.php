<?php

namespace App\Http\Controllers\react_native;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Response;
use Session;

class AppController extends Controller
{

	function downloadApk() {
	    $file= public_path(). "/download/app-debug.apk";

	    $headers = array(
	              'Content-Type: application/pdf',
	            );

	    return Response::download($file, 'app-debug.apk', $headers);
	}

}
