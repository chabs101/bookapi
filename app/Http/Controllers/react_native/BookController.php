<?php

namespace App\Http\Controllers\react_native;
use App\Http\Controllers\Controller;
use App\react_native\Books;
use App\react_native\BooksLocation;
use App\react_native\BooksImageUrl;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Auth;
use DB;

class BookController extends Controller
{
	public function getList() {
		$basepath 		= URL::to('/')."/img/books/";
		return Books::with([
					'bookCategory',
					'bookUser',
					'bookLocation',
					'bookImageUrl' => function($query) use ($basepath) {
						$query->select('book_id','book_image_url_id',DB::raw('CONCAT("'.$basepath.'",image_url) as image_url'));
						// $query->where('book_image_url_id',86);
					}
				])
					->where('deleted_at',null)
					->get();
	}



	public function store(Request $request) {
		

        return	DB::transaction(function() use ($request) {

        	$destinationPath = public_path('/img/books/');
	        $data = request()->except('_token','book_id');
	        $validator = Validator::make(request()->all(), [
	       'book_name' => 'required|unique:books,book_name,'.request()->get('book_id').',book_id',
	       'price' 		=> 'required',
	       'rating' 	=> 'required',
	       'image_url.*' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
	        ]);
	        $success = !$validator->fails();

	        if ($success) {
		           Books::updateOrCreate(['book_id' => request()->get('book_id')],$data);

		           $getbookid = Books::where('book_name',request()->get('book_name'))
		           						->where('price',request()->get('price'))
		           						->where('rating',request()->get('rating'))
		           						->get()
		           						->first();


		           	$bookid = request()->has('book_id') ? request()->get('book_id') : $getbookid->book_id;
		            $bulkimageurl = [];

		                for($i=0; $i<@count(request()->file('image_url')); $i++) {

		                	$img_name = null;
			                if (request()->hasFile('image_url')) {
			                    $img = request()->file('image_url')[$i];
			                    $img_name = $img->getClientOriginalName();
			                    $img->move($destinationPath, $img_name);

			                }

		                    BooksImageUrl::updateOrCreate(
		                    		[
		                                'image_url' 	=> $img_name != null ? $img_name : null,
		                                'book_id'   	=> $bookid
		                            ],
		                            [
		                                "image_url"    	=> $img_name != null ? $img_name : null,
		                                "book_id"      	=> $bookid
		                            ]
		                    );
		                    array_push($bulkimageurl,["image_url","!=",$img_name]);
		                }

		            array_push($bulkimageurl,["book_id","=",$bookid]);
		            BooksImageUrl::where($bulkimageurl)->delete();

		           BooksLocation::updateOrCreate(
			           		[
			           			'book_id' 		=> 	$bookid
			           		],
			           		[
				           		"latitude" 		=> request()->get('latitude'),
				           		"longitude" 	=> request()->get('longitude'),
				           		"user_id" 		=> request()->get('user_id'),
			           		]
					);
		     
					return response()->json([[
								"success" 	=> true
								]]);


	        }else {

		        return response()->json([[
								"success" 	=> false,
								"error" 	=> $validator->errors()
								]]);

	        }

		});


	}



	public function destroy($id) {
        $books = Books::where('book_id',$id)->first();
        if(Books::where('book_id',$id)->delete()){
          
          return response()->json([[
                "success"   => true
                ]]);

        }
          return response()->json([[
              "success"   => false,
              "error"   => "Unable to delete book."
              ]]);
	}
}