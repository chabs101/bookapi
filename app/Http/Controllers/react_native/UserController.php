<?php

namespace App\Http\Controllers\react_native;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use Validator;
use URL;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function getBasicInfo($id) {
        $basepath       = URL::to('/')."/img/users/";

        return User::where('user_id',$id)
                    ->select(
                        'email',
                        'first_name',
                        'last_name',
                        'gender',
                        'isAdmin',
                        DB::raw('CONCAT("'.$basepath.'",image_url) as image_url')
                    )
                    ->get();
    }

    public function store(Request $request) {

        return  DB::transaction(function() use ($request) {
            $data = request()->except('_token','user_id');
            $validator = Validator::make(request()->all(), [
           'email'       => 'required|unique:users',
           'first_name'     => 'required',
           'last_name'      => 'required',
           'gender'         => 'required',
           'isAdmin'        => 'required',
           'image_url'      => 'image|mimes:jpeg,png,jpg,gif|max:2048'
            ]);

            $success = !$validator->fails();
        
            if ($success) {
                $img_name             = $this->imageUrl(request()->file('image_url'));

                $trans                = new User();
                $trans->email      = request()->get('email');
                $trans->password      = Hash::make(12345);
                $trans->first_name    = request()->get('first_name');
                $trans->last_name     = request()->get('last_name');
                $trans->gender        = request()->get('gender');
                $trans->isAdmin       = request()->get('isAdmin');
                $trans->image_url     = $img_name;
                $trans->save();

                return response()->json([[
                                "success"   => true
                                ]]);
            }else {
                return response()->json([[
                                "success"   => false,
                                "error"     => $validator->errors()
                                ]]);
            }

        });

    }

    public function imageUrl($img) {
        $img_name = null;
        $destinationPath = public_path('/img/users/');
        if (request()->hasFile('image_url')) {
            $img = request()->file('image_url');
            $img_name = $img->getClientOriginalName();
            $img->move($destinationPath, $img_name);
        }
            return $img_name;
    }

    public function changeInfo(Request $request) {

        return  DB::transaction(function() use ($request) {
            $data = request()->except('_token','user_id');
            $validator = Validator::make(request()->all(), [
           'email'       => 'required|unique:users,email,'.request()->get('user_id').',user_id',
           'first_name'     => 'required',
           'last_name'      => 'required',
           'gender'         => 'required',
           'isAdmin'        => 'required'
            ]);
// ,email,'.request()->get('user_id').',user_id
            $success = !$validator->fails();
            
            if ($success) {
                $img_name             = $this->imageUrl(request()->file('image_url'));

                $trans                = User::find(request()->get('user_id'));
                $trans->email         = request()->get('email');
                $trans->first_name    = request()->get('first_name');
                $trans->last_name     = request()->get('last_name');
                $trans->gender        = request()->get('gender');
                $trans->isAdmin       = request()->get('isAdmin');
                $trans->image_url     = $img_name;
                $trans->save();

                return response()->json([[
                                "success"   => true
                                ]]);
            }else {
                return response()->json([[
                                "success"   => false,
                                "error"     => $validator->errors()
                                ]]);
            }

        });

    }


    public function changePassword(Request $request) {


        return  DB::transaction(function() use ($request) {
             $messages = [
                'old_password.required' => 'Please enter current password',
                'password_confirmation.required' => 'Confirmation Password doesnt match',
            ];

            $validator = Validator::make(request()->all(), [
                'password' => 'required|confirmed|min:6|max:16',
                'password_confirmation' => 'required|same:password|min:6|max:16',
                'old_password' => 'required'
            ], $messages);

            if (!$validator->fails()) {

                $currentPassword = request()->exists('current_password') ? request()->get('current_password') : Auth::user()->password;
                $oldPassword = request()->get('old_password');

                if (Hash::check(request()->get('password'),$oldPassword)) {
                return response()->json([[
                                "success"   => false,
                                "error"     => "No changes save."
                                ]]);
                }

                if (Hash::check($currentPassword, $oldPassword)) {
                    $user_id = request()->exists('user_id') ? request()->get('user_id') : Auth::user()->user_id;
                    $user = User::find($user_id);
                    $user->password = bcrypt(request()->get('password'));
                    $user->save();

                return response()->json([[
                                "success"   => true
                                ]]);
                }

            }else {

                return response()->json([[
                                "success"   => false,
                                "error"     => $validator->errors()
                                ]]);
            }

        });
        
    }


    public function destroy($id) {
        $User = User::where('user_id',$id)->first();
        if(User::where('user_id',$id)->delete()){
          
          return response()->json([[
                "success"   => true
                ]]);

        }
          return response()->json([[
              "success"   => false,
              "error"   => "Unable to delete user."
              ]]);
    }
}
