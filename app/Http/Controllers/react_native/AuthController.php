<?php

namespace App\Http\Controllers\react_native;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Session;
use Carbon\Carbon;

class AuthController extends Controller
{

	public function authenticate() {
		
		if( Auth::attempt([
			'email' => request()->get('email'), 
			'password' => request()->get('password')
			]) ) {

			$user 			= Auth::user();
			$basepath 		= URL::to('/');
			$image_url 		= $user->image_url !== null ? $basepath.'/img/users/'.$user->image_url :( ($user->gender == "Male") ? $basepath.'/img/dummy-boy.jpg' : $basepath.'/img/dummy-girl.jpg' );
	        $tokenResult = $user->createToken('Personal Access Token');
	        $token = $tokenResult->token;

        if (request()->get('remember_me')) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        $token->save();
 
 			return response()->json([[
						"success" 		=> true,
						"user_id" 		=> $user->user_id,
						"first_name" 	=> $user->first_name,
						"last_name" 	=> $user->last_name,
						"isAdmin" 		=> $user->isAdmin,
						"gender" 		=> $user->gender,
						"image_url" 	=> $image_url,
						"accessToken" 	=> $tokenResult->accessToken,
						"password" 		=> $user->password,
						]]);

		}else {
			return response()->json([[
						"success" 		=> false
						]]);
		}

	}

	public function logout() {
		
		request()->user()->token()->revoke();

		return response()->json([[
					"success" 		=> true
					]]);
	}

}
