<?php

namespace App\Http\Controllers\react_native;
use App\Http\Controllers\Controller;
use App\react_native\BooksCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Auth;
use DB;

class CategoryController extends Controller
{

	public function getList() {
		return BooksCategory::all();
	}

	public function store() {

    $data = request()->except('_token','book_category_id');
    $validator = Validator::make(request()->all(), [
   'category_name' => 
                    [
                      'required',
                      Rule::unique('book_category')->where(function ($query) {
                      $query->where('category_name',request()->get('category_name'))
                            ->where('deleted_at',null);
                      })
                    ]
    ]);
    $success = !$validator->fails();

    if ($success) {

       BooksCategory::updateOrCreate(['book_category_id' => request()->get('book_category_id')],$data);

			return response()->json([[
						"success" 	=> true
						]]);
      
    }

    return response()->json([[
				"success" 	=> false,
				"error" 	=> $validator->errors()
				]]);;

	}

	public function destroy($id) {
    $category = BooksCategory::where('book_category_id',$id)->first();
    if(BooksCategory::where('book_category_id',$id)->delete()){
      
      return response()->json([[
            "success"   => true
            ]]);

    }
      return response()->json([[
          "success"   => false,
          "error"   => "Unable to delete category."
          ]]);
	}
}