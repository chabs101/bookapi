<?php

namespace App\react_native;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class BooksCategory extends Model
{
    use SoftDeletes;
    	
	protected $table 		= 'book_category';
	protected $primaryKey 	= 'book_category_id';
    protected $fillable = [
				'category_name',
	];
}