<?php

namespace App\react_native;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class BooksLocation extends Model
{
    use SoftDeletes;
    	
	protected $table 		= 'books_location';
	protected $primaryKey 	= 'book_location_id';
    protected $fillable = [
				'latitude',
				'longitude',
				'user_id',
				'book_id',
	];
}