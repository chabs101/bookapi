<?php

namespace App\react_native;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
	protected $table 		= 'messages';
	protected $primaryKey 	= 'message_id';
    protected $fillable = [
				'message',
				'sender_id',
				'receiver_id',
				'seen_at',
	];
}
