<?php

namespace App\react_native;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class BooksImageUrl extends Model
{
    use SoftDeletes;
    	
	protected $table 		= 'books_image_url';
	protected $primaryKey 	= 'book_image_url_id';
    protected $fillable = [
				'image_url',
				'book_id',
	];
}