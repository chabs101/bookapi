<?php

namespace App\react_native;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    use SoftDeletes;
    	
	protected $table 		= 'books';
	protected $primaryKey 	= 'book_id';
    protected $fillable = [
				'book_name',
				'price',
				'rating',
				'book_category_id',
				'user_id',
	];

	public function bookLocation() {
    	return $this->belongsTo(\App\react_native\BooksLocation::class, 'book_id');
    }

	public function bookUser() {
    	return $this->belongsTo(\App\User::class, 'user_id');
    }

	public function bookCategory() {
    	return $this->belongsTo(\App\react_native\BooksCategory::class, 'book_category_id');
    }

	public function bookImageUrl() {
    	return $this->hasMany(\App\react_native\BooksImageUrl::class, 'book_id');
    }
    
}